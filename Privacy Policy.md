**Introduction** 

At SbrConnect, we know you care about your personal information, so we have prepared this privacy policy (our “Privacy Policy”) to explain how we collect, use and share it. 
 
**Your consent**  

By using or accessing our Apps (Services), you agree to the terms of this Privacy Policy. 
 
**What information do we collect?** 

We **DO NOT** collect, store or use any personal information while you visit, download or upgrade our products. 
In our apps, we must require some permissions such as: RECORD_AUDIO, VIEW_NETWORK_CONNECTIONS and FULL_NETWORK_ACCESS. 

* All Audio Recordings are stored in your devices. We **DO NOT** collect, store or use them. 
* Network Connections permissions allow users to share Apps with their contacts. We can't and don't access that information. 
* Google, via AdMob, as a third party vendor, uses cookies to serve ads on our Services. Google advertisement services collect some sort of personally identifiable information to make sure they are able track the effectivity of their ads or to increase their reach and impact. Users may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy. By using our application, users are bound by Google's Terms of Service. Within the site we use the Google Maps API(s) and incorporate by reference the Google privacy policy by including a link to the Google privacy policy (currently http://www.google.com/privacy.html), as amended by Google from time to time. Google reserves the right to make changes to their Terms from time to time. When these changes are made, Google will make a new copy of the Terms available at http://code.google.com/apis/maps/terms.html (or such successor URLs that Google may designate from time to time).  
Google's Privacy Policy. For information about Google's data protection practices, please read Google's privacy policy at http://www.google.com/privacy.html. This policy explains how Google treats your personal information and protects your privacy when you use the Service. 
 
**Do we disclose any information to outside parties?**  

We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. This does not include trusted third parties who assist us in operating our Services, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others rights, property, or safety.  
 
**Links** 

Our Services may contain links to other websites. We are not responsible for the privacy practices of other websites. We encourage users to be aware when they leave the Service to read the privacy statements of other websites that collect personally identifiable information. This Privacy Policy applies only to information collected by SbrConnect via the Service. 
 
**Changes to our Privacy Policy**  

If we decide to change our privacy policy, we will post those changes on this page. 
 
**Contact Us** 

To understand more about our Privacy Policy, access your information, or ask questions about our privacy practices or issue a complaint, please contact us at sbrconnect@gmail.com 